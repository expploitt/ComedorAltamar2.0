/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comedoraltamar;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author expploitt
 */
public class ComedorAltamar implements Runnable {

    private final int PORT = 2045;
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        JVentana ventana = new JVentana();
        
        /*CREAMOS EL HILO PARA LA ESCUCHA*/
        Runnable comedorAltamar = new ComedorAltamar();
        new Thread(comedorAltamar).start();

    }

    /**
     * Hilo de escucha de peticiones de clientes para apuntarse o borrarse en
     * las distintas comidas
     */
    @Override
    public void run() {

        try {

            ServerSocket socket = new ServerSocket(PORT);

            while (true) {
                System.out.println("comedor.altamar.ComedorAltamar.run()");
                Socket cliente = socket.accept();
                System.out.println("Conectado!");
                /*CREAMOS EL HILO*/
                Runnable thread = new Hilo(cliente);
                new Thread(thread).start();

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    
}
